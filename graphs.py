#!/usr/bin/env python3

"""
Module used to analyse a binary file, using kind of an UI
"""

import argparse
import logging

import angr

from angrlab.analyses_res import res_analyse, RES_FUNCS, analyses
from angrlab.cli import prompt_analyses, prompt_symbols, prompt_objs


def get_parser():
    """
    Returns the command line argument parser.
    """
    parser = argparse.ArgumentParser(description="Angr static analyses.")
    parser.add_argument("binary", help="Binary file to analyse", type=str)
    parser.add_argument(
        "-v", "--verbose", help="increase verbosity level", action="count", default=0
    )
    return parser


def set_logger_lvl(lvl):
    """
    Set the logging level for angrlab based on param -v(v).
    """
    logger = logging.getLogger("angrlab")
    if lvl >= 2:
        logger.setLevel(logging.DEBUG)
    elif lvl >= 1:
        logger.setLevel(logging.INFO)


def main():
    """
    Main function.
    """
    parser = get_parser()
    args = parser.parse_args()
    filename = args.binary
    set_logger_lvl(args.verbose)

    proj = angr.Project(filename, load_options={"auto_load_libs": False})

    prompt_objs(proj)
    addr = prompt_symbols(proj.loader.main_object)
    names = prompt_analyses(RES_FUNCS.keys())

    cfg, cfgfast, cdg, ddg = analyses(proj, addr)

    for name in names:
        res_analyse(name, cfg=cfg, cfgfast=cfgfast, cdg=cdg, ddg=ddg, proj=proj)


if __name__ == "__main__":
    main()

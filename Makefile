all: bin

bin:
	$(MAKE) -C examples

.PHONY: clean doc

clean:
	rm -f *.gv
	rm -f *.gv.pdf
	rm -rf */__pycache__
	$(MAKE) -C examples clean
doc:
	$(MAKE) -C doc html

format:
	autoflake -r -i --remove-all-unused-imports --ignore-init-module-imports angrlab
	isort -rc angrlab
	black angrlab

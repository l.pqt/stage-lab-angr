Graphs (DOT)
============

Generic graphs
--------------

.. automodule:: angrlab.dot.graph
   :members:

CFG
---

.. automodule:: angrlab.dot.cfg
   :members:

CDG
---

.. automodule:: angrlab.dot.cdg
   :members:

DDG (statements)
----------------

.. automodule:: angrlab.dot.ddg
   :members:

DDG (data)
----------

.. automodule:: angrlab.dot.data_ddg
   :members:

Callgraph
---------

.. automodule:: angrlab.dot.callgraph
   :members:

Paths
-----

.. automodule:: angrlab.dot.paths
   :members:

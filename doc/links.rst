Links / Resources
=================

http://angr.io/

Github repos:

- https://github.com/season-lab/survey-symbolic-execution
- https://github.com/ksluckow/awesome-symbolic-execution
- https://github.com/ucsb-seclab/BootStomp

Others:

- https://www.technovelty.org/linux/plt-and-got-the-key-to-code-sharing-and-dynamic-libraries.html
- https://www.owasp.org/index.php/Static_Code_Analysis

References:

.. bibliography:: biblio.bib
   :all:
   :list: bullet

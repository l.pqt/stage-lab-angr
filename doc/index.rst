.. AngrLab documentation master file, created by
   sphinx-quickstart on Thu Jun 27 14:36:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../README.md

.. toctree::
   :hidden:
   :maxdepth: 2

   graphs
   cli
   dataframes
   plugins
   analyses
   stubs
   utilities
   links

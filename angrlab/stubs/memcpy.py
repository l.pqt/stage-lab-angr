"""
Module conaining the memcpy SimProcedure

memcpy is a wrapper around the default memcpy with more checks
"""
import logging

import angr

from angrlab.plugins.vuln import VulnCategory, VulnReport, VulnSeverity
from angrlab.utils import is_allocated

LOGGER = logging.getLogger(__name__)


# pylint: disable=invalid-name,too-few-public-methods
class memcpy(angr.SIM_PROCEDURES["libc"]["memcpy"]):
    """
    A "secured" version of memcpy
    """

    def run(self, dst, src, limit):
        """
        Runs the simprocedure, checks if destination has enough memory allocated

        :param dst: Destination address
        :param src: Source address
        :param limit: Size to be copied
        """
        LOGGER.info(f"dst {dst}, src {src}, size {limit}")

        solver = self.state.solver
        addr_dst = solver.eval(dst)

        write_sizes = is_allocated(addr_dst, self.state)

        # warning if no dynamic alloc
        if write_sizes == []:
            LOGGER.error("Destination buffer wasn't allocated ?")
            return super().run(dst, src, limit)

        # The first of the list is the oldest (first to be executed)
        nb_bytes_dst = write_sizes[0]
        LOGGER.debug(f"Allocated {nb_bytes_dst}")

        # Run memcpy
        result = super().run(dst, src, limit)

        vuln_plugin = self.state.project.kb.vuln

        # If we are actually performing an invalid write
        if solver.is_true(limit > nb_bytes_dst):
            LOGGER.warning(
                f"Destination buffer size ({nb_bytes_dst}) is less than limit ({limit})"
            )
            vuln_plugin.register(
                VulnReport(
                    VulnCategory.BUFFER_OVERFLOW,
                    VulnSeverity.SURE,
                    self.state.copy(),
                    source_variable=limit,
                )
            )

        # if we may perform an invalid write
        elif solver.satisfiable(extra_constraints=[limit > nb_bytes_dst]):
            LOGGER.warning(
                f"Destination buffer size ({nb_bytes_dst}) may be less than limit ({limit})"
            )

            errored_state = self.state.copy()

            # We add the constraint to the new state
            errored_state.solver.add(limit > nb_bytes_dst)

            vuln_plugin.register(
                VulnReport(
                    VulnCategory.BUFFER_OVERFLOW,
                    VulnSeverity.LIKELY,
                    errored_state,
                    source_variable=limit,
                )
            )

        return result

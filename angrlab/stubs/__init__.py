"""
Module that stores everything that concerns stubs, and SimProcedure,
allong with some utilities to handle them
"""
from .arm_restub import arm_restub
from .calloc import calloc
from .div import div
from .malloc import malloc
from .memcpy import memcpy

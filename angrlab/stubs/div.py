"""
Module containing a "secured" version of the division as a SimProcedure

It checks that the second operand is not zero
"""
import logging

import angr

from angrlab.plugins.vuln import VulnCategory, VulnReport, VulnSeverity

LOGGER = logging.getLogger(__name__)


# pylint: disable=invalid-name,too-few-public-methods
class div(angr.SimProcedure):
    """
    A secured version of the division that checks that the second operand is not zero
    """

    # pylint: disable=arguments-differ
    def run(self, arg1, arg2):
        """
        Runs the simprocedure, checks is not ZeroDiv are possible

        :param arg1: The numerator
        :param arg2: The denominator
        """
        LOGGER.debug(f"Division {arg1} / {arg2}")
        solver = self.state.solver
        addr = self.state.callstack.call_site_addr
        vuln_plugin = self.state.project.kb.vuln

        if solver.is_true(arg2 == 0):
            LOGGER.warning(
                f"{arg2.ast} will always cause a zero division at {addr:#x}."
            )
            vuln_plugin.register(
                VulnReport(
                    VulnCategory.ZERO_DIV,
                    VulnSeverity.SURE,
                    self.state.copy(),
                    source_variable=arg2.ast,
                )
            )

        elif solver.satisfiable(extra_constraints=[arg2 == 0]):
            LOGGER.warning(f"{arg2.ast} may cause a zero division at {addr:#x}.")
            errored_state = self.state.copy()

            errored_state.solver.add(arg2 == 0)
            vuln_plugin.register(
                VulnReport(
                    VulnCategory.ZERO_DIV,
                    VulnSeverity.LIKELY,
                    errored_state,
                    source_variable=arg2.ast,
                )
            )

        return arg1 / arg2

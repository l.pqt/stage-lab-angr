"""
Module containing a "secured" version of malloc
"""
import logging

import angr

from .common import store_nb_bytes

LOGGER = logging.getLogger(__name__)


# pylint: disable=invalid-name,too-few-public-methods
class malloc(angr.SIM_PROCEDURES["libc"]["malloc"]):
    """
    A "secured" version of malloc that stores the number of bytes allocated
    """

    def run(self, sim_size):
        """
        Runs the simprocedure, concretize the returned

        :param sim_size: Size to be allocated
        """
        LOGGER.info(f"size {sim_size}")
        res = super().run(sim_size)
        store_nb_bytes(self, res, sim_size)
        return res

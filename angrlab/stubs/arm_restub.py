"""
Contains functions to automatically change the stubs for an (ARM) project
"""
import logging

import angr

from .calloc import calloc
from .div import div
from .malloc import malloc
from .memcpy import memcpy

LOGGER = logging.getLogger(__name__)


def arm_restub(proj: angr.Project) -> None:
    """
    Restub some libc functions with our own stub to apply analyses.

    :param proj: the angr Project to restub
    """
    stub_dict = {
        "__aeabi_fdiv": div,
        "__aeabi_idiv": div,
        "memcpy": memcpy,
        "calloc": calloc,
        "malloc": malloc,
    }

    for symbol, stub in stub_dict.items():
        if symbol in map(lambda sym: sym.name, proj.loader.symbols):
            LOGGER.info(f"Rehooking {symbol} with {stub.__name__}")
            proj.hook_symbol(symbol, stub(), replace=True)

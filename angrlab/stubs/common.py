"""
Common functions for stubs.
"""

from angr import SimProcedure


def store_nb_bytes(simproc: SimProcedure, addr: int, size: int) -> None:
    """
    Stores in the state the number of bytes allocated at the given address

    :param simproc: The simprocedure that called
    :type simproc: SimProcedure
    :param addr: The address at which the store occurs
    :type addr: int
    :param size: The size allocated
    :type size: int
    """
    solver = simproc.state.solver
    addr = simproc.state.solver.eval(addr)
    nb_bytes = solver.eval(size)
    dict_to_add = {addr: nb_bytes}
    if simproc.state.globals.get("nb_bytes"):
        simproc.state.globals["nb_bytes"].update(dict_to_add)
    else:
        simproc.state.globals["nb_bytes"] = dict_to_add

"""
Module containing a secured version of calloc
"""
import logging

import angr

from .common import store_nb_bytes

LOGGER = logging.getLogger(__name__)


# pylint: disable=invalid-name,too-few-public-methods
class calloc(angr.SIM_PROCEDURES["libc"]["calloc"]):
    """
    A "secured" version of calloc, that stores the number of bytes allocated
    """

    def run(self, sim_nmemb, sim_size):
        """
        Runs the simprocedure, performs a calloc and concretize the returned address

        :param sim_nmemb: number of members
        :param sim_size: Size of one member
        """
        LOGGER.info(f"addr {sim_nmemb}, size {sim_size}")
        res = super().run(sim_nmemb, sim_size)
        store_nb_bytes(self, res, sim_size * sim_nmemb)
        return res

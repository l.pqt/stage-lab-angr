"""
Module to centralize all command line input/output with user.
"""

from typing import List

import angr
import cle

from angrlab.dataframes import df_analyses, df_functions, df_objs, df_symbols


def prompt_input(label: str) -> str:
    """
    Prompt for user input.

    :param label: question to display for prompt
    :return: result of input
    """
    return input(f"\n{label}:\n> ")


def title(label: str, width: int = 80, char: str = "-") -> None:
    """
    Display a centered title.

    :param label: title
    :param width: width of the title
    :param char: character to use
    """
    string = " " + label + " "
    print(string.center(width, char))


def prompt_objs(proj: angr.project.Project) -> None:
    """
    Display all objects in the given project.

    :param proj: angr project
    """
    title(f"Objects in {proj.filename}")
    print(df_objs(proj))
    print()


def prompt_symbols(obj: cle.backends.Backend) -> int:
    """
    Display all symbols in the given object and return the choosen one.

    :param obj: CLE object
    :return: address of the choosen symbol
    """
    title(f"Local symbols")
    print(df_symbols(obj).to_string())
    symbol = prompt_input("Symbol")
    addr = obj.symbols[int(symbol)].rebased_addr if symbol else None
    print()
    return addr


def prompt_analyses(analyses: List[str]) -> List[str]:
    """
    Display all available analyses and return list of choosen ones.

    :param analyses: list of available analyses names
    :return: list of choosen names
    """
    title("Analyses")
    dataframe = df_analyses(analyses)
    print(dataframe)
    ids = [int(i) for i in prompt_input("Analyse(s)").split()]
    if ids == []:
        ids = [i for i in range(len(dataframe.index))]
    names = [dataframe["analyses"].values.tolist()[i] for i in ids]
    print()
    return names


def prompt_ddg_reg_exclude() -> List[str]:
    """
    Ask user to choose registers to exclude for DDG andd return them.

    :return: list of registers names to exclude
    """
    default = ["sp", "pc", "lr", "r11"]
    excluded_str = prompt_input(f"Registers to exclude in DDG, default {default}")
    if excluded_str == "":
        excluded = default
    else:
        excluded = excluded_str.replace(" ", "").split(",")
    return excluded


def prompt_fn_path(proj: angr.project.Project) -> angr.knowledge_plugins.Function:
    """
    Display all functions and ask the one to use for path.

    :param proj: angr project to use
    :return: function (from angr knowledge_plugins)
    """
    title("Functions")
    funcs = list(proj.kb.functions.values())
    print(df_functions(funcs))
    fn_id = int(prompt_input("Function"))
    return funcs[fn_id]

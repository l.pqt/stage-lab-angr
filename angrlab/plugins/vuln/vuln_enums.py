"""
Useful enumerations for the VulnReport
"""
from enum import Flag, IntEnum, auto


class VulnSeverity(IntEnum):
    """
    Enum to register the severity (likelyhood) of a vulnerability
    the higher it gets, the likelier
    """

    LIKELY = 1
    SURE = 2
    CONTROLED = 3


class VulnCategory(Flag):
    """
    Registers the category of a vulnerability
    Can be used to identify a vulnerability and generate exploits afterwards.
    """

    ZERO_DIV = auto()
    BUFFER_OVERFLOW = auto()
    SYMBOLIC_JUMP = auto()

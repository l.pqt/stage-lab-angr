"""
A module to store VulnPlugin (a kb plugin) and VulnReport

VulnPlugin is a container of VulnReports with some useful list-like interface
"""
import logging
from collections import defaultdict
from itertools import chain
from pprint import pformat

import angr

from .vuln_enums import VulnCategory, VulnSeverity

LOGGER = logging.getLogger(__name__)


class VulnReport:
    """
    A vulnerability report

    This is just a container used to retain category and severity (or likelyhood)
    and the state leading to the vulnerability

    :param category: the VulnCategory coresponding to this vulnerability
    :param severity: the VulnSeverity corresponding to ths vulnerability
    :param state: the SimState at which the vulnerability is caused
    :param source_variable: the variable causing the vulnerability
    :param kwargs: all extra informations on the vulnerability
    """

    def __init__(
        self,
        category: VulnCategory,
        severity: VulnSeverity,
        state: angr.SimState,
        source_variable=None,
        **extras,
    ):
        """
        Takes kwargs as extra informations about the vulnerability
        """
        self.category = category
        self.severity = severity
        self.state = state
        self.source_variable = source_variable
        self.extras = extras

    @property
    def stdin(self) -> bytes:
        """
        Stdin an input leading to the error
        """
        return self.state.posix.dumps(0)

    @property
    def stdout(self) -> bytes:
        """
        Output at the moment of the error
        """
        return self.state.posix.dumps(1)

    @property
    def callstack(self):
        """
        Callstack at the moment of the error
        """
        return self.state.callstack

    @property
    def block_addr(self) -> int:
        """
        block address of the error
        """
        return self.state.addr

    @property
    def block(self) -> angr.block.Block:
        """
        Block containing the error
        """
        return self.state.project.factory.block(self.block_addr)

    @property
    def func_addr(self) -> int:
        """
        Addres of the function where the error is
        """
        return self.state.callstack.current_function_address

    def copy(self):
        """
        Returns a deep copy of self
        """
        return VulnReport(
            self.category,
            self.severity,
            self.state.copy(),
            self.source_variable,
            **self.extras.copy(),
        )

    @property
    def summary(self) -> str:
        """
        Full summary of the error/vulnerability
        """
        sep = "\n{:=^50}\n"
        summary_string = str(self) + f" ==> {self.severity.name}\n"

        summary_string += sep.format(" LOCATION ")
        summary_string += f"Function : {hex(self.func_addr)}\n"
        summary_string += f"Block : {hex(self.block_addr)}\n"

        summary_string += "\nSource block:\n"
        summary_string += str(self.block.capstone) + "\n"

        summary_string += "\n" + str(self.callstack) + "\n"

        summary_string += sep.format(" ARGUMENTS ")

        summary_string += "\nstdin:\n"
        summary_string += pformat(self.stdin) + "\n"

        summary_string += "\nstdout:\n"
        summary_string += pformat(self.stdout) + "\n"

        summary_string += "\nSource variable:\n"
        if self.source_variable is not None:
            summary_string += str(self.source_variable) + " = "
            summary_string += str(self.state.solver.eval(self.source_variable)) + "\n"
        else:
            summary_string += "None\n"

        summary_string += "\nVariables:\n"
        for tup, var in self.state.solver.get_variables():
            summary_string += (
                self._get_var_name(tup)
                + " = "
                + hex(self.state.solver.eval(var))
                + "\n"
            )

        summary_string += sep.format(" CONSTRAINTS ") + "\n"
        for constraint in self.state.solver.constraints:
            summary_string += f"{str(constraint)}\n"

        summary_string += sep.format(" EXTRAS ") + pformat(self.extras) + "\n"

        return summary_string

    def pprint(self) -> None:
        """
        Prints the full summary of the error
        """
        print(self.summary)

    def _get_var_name(self, tup: tuple) -> str:
        """
        Returns a pretty-printable version of an identifier
        as state.sorted.get_variables returns
        """
        var_type = tup[0]

        if var_type == "reg":
            return self.state.arch.translate_register_name(tup[1])
        if var_type == "mem":
            return "mem @ " + hex(tup[1])
        if var_type == "api":
            api_call = tup[1]
            if api_call == "scanf":
                return api_call + "[" + tup[3].decode() + "]"
        if var_type == "file":
            file_type = tup[1]
            if file_type == "stdin":
                return file_type
        return str(tup)

    def __str__(self):
        return f"Vuln @ {repr(self.state)} : {self.category.name}"

    def __repr__(self):
        return f"<{str(self)}>"

    def __eq__(self, other):
        return (
            self.block_addr == other.block_addr
            and self.category == other.category
            and self.severity == other.severity
        )

    def __hash__(self):
        return hash((self.block_addr, self.category, self.severity))


class VulnPlugin(angr.knowledge_plugins.plugin.KnowledgeBasePlugin):

    """
    KnowledgeBase plugin used to register vulnerabilities
    will be used with a custom analisis to register (and possibly exploit)
    vulnerabilities in the project, and exposing vulnerabilities sorted by function addresses
    """

    def __init__(self, kb):
        super(VulnPlugin, self).__init__()
        self._registered = defaultdict(set)
        self._kb = kb

    def register(self, vuln: VulnReport):
        """
        Registers a new vulnerability as a VulnReport
        """
        LOGGER.debug(f"Registering {vuln} in {self}")
        self._registered[vuln.func_addr].add(vuln)

    def copy(self):
        """
        Returns a deep copy of self
        """
        new = VulnPlugin(self._kb)
        for vuln in self:
            new.register(vuln.copy())

    def keys(self):
        """
        Return all the keys
        """
        return self._registered.keys()

    def __contains__(self, item):
        return item in self._registered

    def __getitem__(self, item):
        return list(self._registered[item])

    def __iter__(self):
        """
        Iterates over all vulnerabilites
        """
        yield from chain(*self._registered.values())

    def __repr__(self):
        return f"<Vulnerabilities : {list(self._registered.items())}>"

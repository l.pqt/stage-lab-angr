"""
Module that contains the vuln kb plugin

Importing it registers the kb plugin to the kb as default
"""
from .vuln_enums import VulnCategory, VulnSeverity
from .vuln_plugin import VulnPlugin, VulnReport

VulnPlugin.register_default("vuln", VulnPlugin)

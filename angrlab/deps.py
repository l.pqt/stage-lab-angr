"""
Little module to gather dependencies between arguments and return values
"""

import logging
from itertools import product
from typing import Set

import angr
from angr.sim_variable import SimRegisterVariable
from networkx.classes.digraph import DiGraph

from angrlab.ddg_simplifier import ddg_simplifier, simvar_pp_str
from angrlab.utils import get_datapaths, pairwise

LOGGER = logging.getLogger(__name__)


def get_args_locations(
    ddg: angr.analyses.ddg.DDG, func: angr.knowledge_plugins.Function
) -> Set[angr.analyses.code_location.CodeLocation]:
    """
    Gets the args location as CodeLocation for the given function.

    This is just gathering the first value of the arguments registers in the function.

    :param ddg: a ddg that go throught the function
    :param func: the function

    :return: A list of possible args values
    """

    if func.num_arguments == 0 and func.arguments == []:
        LOGGER.warning(f"Not args found for {func.name}.")
        return set()

    args_registers = func.arguments

    LOGGER.info(f"Found {len(args_registers)} args for {func.name}.")

    args_prog_var_locations = set()

    for reg_id in args_registers:

        reg_occurences = list(
            filter(
                lambda node: isinstance(node.variable, SimRegisterVariable)
                and node.variable.reg == reg_id  # pylint: disable=cell-var-from-loop
                and node.location.ins_addr is not None
                and node.location.ins_addr >= func.addr,
                ddg.data_graph,
            )
        )

        LOGGER.debug(
            f"Found {len(reg_occurences)} of reg {reg_id} occurences : {reg_occurences}"
        )

        sorted_reg_occurences = sorted(
            reg_occurences, key=lambda node: node.location.ins_addr
        )

        args_prog_var_locations.add(sorted_reg_occurences[0])
        LOGGER.info(f"Found arg location : {sorted_reg_occurences[0]}")

    return args_prog_var_locations


def get_return_location(
    cfg: angr.analyses.cfg.CFG,
    ddg: angr.analyses.ddg.DDG,
    func: angr.knowledge_plugins.Function,
) -> Set[angr.analyses.code_location.CodeLocation]:
    """
    Returns ProgramVariables corresponding to possible return locations of current fonction.

    This is just looking at the return locations of the function, and getting the last value of the return register.

    :param cfg: A cfg going throught the function
    :param ddg: A ddg going throught the function
    :param func: The function to analyse

    :return: the list of possible return values
    """

    LOGGER.info(f"Found {len(func.ret_sites)} rets for {func.name}.")

    ret_possible_locations = set()

    default_return_register = cfg.project.arch.register_list[0].vex_offset

    for block in map(lambda node: cfg.get_any_node(node.addr).block, func.ret_sites):

        reg_occurences = list(
            filter(
                lambda node: isinstance(node.variable, SimRegisterVariable)
                and node.variable.reg == default_return_register
                and node.location.ins_addr is not None
                and node.location.ins_addr
                <= max(block.instruction_addrs),  # pylint: disable=cell-var-from-loop
                ddg.data_graph,
            )
        )

        LOGGER.debug(
            f"Found {len(reg_occurences)} of reg {default_return_register} occurences : {reg_occurences}"
        )

        sorted_reg_occurences = sorted(
            reg_occurences, key=lambda node: node.location.ins_addr, reverse=True
        )

        ret_possible_locations.add(sorted_reg_occurences[0])
        LOGGER.info(f"Found ret location : {sorted_reg_occurences[0]}")

    return ret_possible_locations


def get_deps_func(
    cfg: angr.analyses.cfg.CFG,
    ddg: angr.analyses.ddg.DDG,
    func: angr.knowledge_plugins.Function,
) -> DiGraph:
    """
    Gets all paths between arguments and return values.

    :param cfg: a cfg going throught the function
    :param ddg: a ddg going throught the function
    :param func: The function to analyse

    :return: a Directed Graph representing the dependencies
    """

    args = get_args_locations(ddg, func)

    rets = get_return_location(cfg, ddg, func)

    simple_ddg = ddg_simplifier(ddg, cfg)

    new_graph = DiGraph()

    for arg in args:
        new_graph.add_node(arg, pp_str=simvar_pp_str(arg, ddg, cfg))

    for i, ret in enumerate(rets):
        new_graph.add_node(ret, pp_str=f"Ret {i}\n" + simvar_pp_str(ret, ddg, cfg))

    for arg, ret in product(args, rets):
        count = 0

        for path in get_datapaths(simple_ddg, arg, ret):
            for node1, node2 in pairwise(path):
                edge_type = simple_ddg.get_edge_data(node1, node2)["type"]

                if node1 not in new_graph:
                    new_graph.add_node(node1, pp_str=simvar_pp_str(node1, ddg, cfg))
                if node2 not in new_graph:
                    new_graph.add_node(node2, pp_str=simvar_pp_str(node2, ddg, cfg))

                new_graph.add_edge(node1, node2, type=edge_type)

        LOGGER.info(f"Found {count} paths bewteen {arg} and {ret}")

    return new_graph

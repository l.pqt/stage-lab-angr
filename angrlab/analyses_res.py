"""
Module used by angrlab.cli to produce all of its analyses
"""
import angr

from angrlab.cli import prompt_ddg_reg_exclude, prompt_fn_path
from angrlab.ddg_simplifier import ddg_simplifier
from angrlab.deps import get_deps_func
from angrlab.dot import (
    dot_callgraph,
    dot_cdg,
    dot_cfg,
    dot_data_ddg,
    dot_ddg,
    draw_graph,
)


def analyses(proj: angr.project.Project, start_addr=None, vex_opt_level: int = -1):
    """
    Compute CFG, CDG, and DDG, over proj, uses by default no
    vex optimization, change vex_opt_level to gain performance
    """
    if start_addr:
        starts = [start_addr]
        start = start_addr
    else:
        starts = None
        start = None

    cfgfast = proj.analyses.CFGFast(normalize=True)

    cfg = proj.analyses.CFGEmulated(
        keep_state=True,
        starts=starts,
        state_add_options=angr.options.refs,
        iropt_level=vex_opt_level,
        normalize=True,
    )

    cdg = proj.analyses.CDG(cfg)
    ddg = proj.analyses.DDG(cfg, start)
    return cfg, cfgfast, cdg, ddg


def res_cfg_asm(**kwargs):
    """
    Plots the cfg of the current project (in asm)
    """
    cfg = kwargs.get("cfg")
    dot_cfg(cfg)


def res_cfg_vex(**kwargs):
    """
    Plots the cfg of the current project (is VEX)
    """
    cfg = kwargs.get("cfg")
    dot_cfg(cfg, use_asm=False, filename="cfg_vex.gv")


def res_cfgfast(**kwargs):
    """
    Plots a CFG fast of current cfg (in asm)
    It can sometimes be more accurate than traditionnal cfg
    """
    cfgfast = kwargs.get("cfgfast")
    dot_cfg(cfgfast, filename="cfgfast.gv")


def res_cdg(**kwargs):
    """
    Plots the cdg of the current project
    """
    cfg = kwargs.get("cfg")
    cdg = kwargs.get("cdg")
    dot_cdg(cdg, cfg)


def res_ddg_stmts(**kwargs):
    """
    Plots the data depency between statements of the current project
    """
    ddg = kwargs.get("ddg")
    dot_ddg(ddg)


def res_ddg_data(**kwargs):
    """
    Plots a simplified version of the data dependency graph
    See ddg_simplifier
    """
    cfg = kwargs.get("cfg")
    ddg = kwargs.get("ddg")
    excluded_regs = prompt_ddg_reg_exclude()
    simple_ddg = ddg_simplifier(ddg, cfg, exclude_registers=excluded_regs)
    dot_data_ddg(simple_ddg)


def res_callgraph(**kwargs):
    """
    Returns the callgraph of the current project
    """
    cfg = kwargs.get("cfg")
    dot_callgraph(cfg)


def res_path(**kwargs):
    """
    Diplays the data dependencies between arguments and return
    values of a the prompted function
    """
    proj = kwargs.get("proj")
    cfg = kwargs.get("cfg")
    ddg = kwargs.get("ddg")

    func = prompt_fn_path(proj)
    paths = get_deps_func(cfg, ddg, func)
    dot_data_ddg(paths)


def res_ddg_full(**kwargs):
    """
    Displays the ddg in full version,
    used to debug ddg_simplifier and others
    """
    ddg = kwargs.get("ddg")
    draw_graph(ddg.data_graph).render("data_ddg_debug.gv", view=True)


RES_FUNCS = {
    "CFG (asm)": res_cfg_asm,
    "CFG (vex)": res_cfg_vex,
    "CFGFast": res_cfgfast,
    "CDG": res_cdg,
    "DDG (stmts)": res_ddg_stmts,
    "DDG (data)": res_ddg_data,
    "Callgraph": res_callgraph,
    "Path": res_path,
    "DDG full (debug)": res_ddg_full,
}


def res_analyse(name, **kwargs):
    """
    Performs the analysis corresponding to the given name, passing **kwargs

    :param name: The name of the analisis
    :type name: str
    :param kwargs: The arguments that are going to be passed to the analysis
    """
    func = RES_FUNCS[name]
    func(**kwargs)

"""
This module return informations as (pandas) dataframe for pretty-printing.
"""

import pandas as pd


def df_objs(proj):
    """
    Return a dataframe of all objects in the project.
    """
    objs = proj.loader.shared_objects
    names = ["name", "min addr", "max addr", "entry", "is main"]
    data = [
        (
            key,
            hex(o.min_addr),
            hex(o.max_addr),
            hex(o.entry),
            "✓" if o is proj.loader.main_object else "",
        )
        for key, o in objs.items()
    ]
    return pd.DataFrame(data, columns=names)


def df_symbols(obj, restrict=False):
    """
    Return a dataframe of all symbols in an given object.
    """
    symbols = [s for s in obj.symbols]
    if restrict:
        symbols = filter(lambda s: s.is_function and not s.is_import, symbols)
    names = [
        "name",
        "rebased addr",
        "linked addr",
        "relative addr",
        "function",
        "import",
    ]
    data = [
        (
            s.demangled_name,
            hex(s.rebased_addr),
            hex(s.linked_addr),
            hex(s.relative_addr),
            "✓" if s.is_function else "",
            "✓" if s.is_import else "",
        )
        for s in symbols
    ]
    return pd.DataFrame(data, columns=names)


def df_analyses(analyses):
    """
    Return a dataframe of all the available analyses.
    """
    names = ["analyses"]
    return pd.DataFrame(analyses, columns=names)


def df_functions(functions):
    """
    Return a dataframe of all functions with their address and number of arguments.
    """
    names = ["name", "addr", "args_nr"]
    data = [(f.name, hex(f.addr), len(f.arguments)) for f in functions]
    return pd.DataFrame(data, columns=names)

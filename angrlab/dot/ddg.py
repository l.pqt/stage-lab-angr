"""
Module to draw Data Dependency Graph between statements (VEX).
"""

import logging

from angrlab.utils import get_inst

from .graph import draw_graph

LOGGER = logging.getLogger(__name__)


def _key(node):
    return str(node.ins_addr)


def _node(dot, node, data, key, **kwargs):
    project = kwargs.get("project", None)
    label = ""
    if node.sim_procedure:
        label = str(node.sim_procedure)
    else:
        inst = get_inst(project, node.ins_addr)
        label = str(inst)
    LOGGER.debug(f"node: {data}")
    dot.node(key(node), label)


def _edge(dot, src, dst, data, key, **kwargs):
    edges_done = kwargs.get("edges_done", None)
    LOGGER.info(f"{src} {dst} {data}")
    key1 = key(src)
    key2 = key(dst)
    if key1 != key2 and (key1, key2) not in edges_done:
        edges_done.add((key1, key2))
        color = "black"
        if data["type"] == "mem":
            color = "red"
        elif data["type"] == "reg":
            color = "blue"
        elif data.get("subtype"):
            color = "green"
        dot.edge(key1, key2, color=color)


def dot_ddg(ddg, filename="ddg.gv", view=True):
    """
    Draw DDG between statements.

    - Black: default
    - Red: mem
    - Blue: reg
    - Green: subtype defined
    """
    edges_done = set()
    dot = draw_graph(
        ddg.graph, _node, _edge, _key, project=ddg.project, edges_done=edges_done
    )
    dot.render(filename, view=view)

"""
Module to draw Control Dependency Graph
"""

import logging

import angr

from angrlab.utils import node_to_str

from .graph import draw_graph

LOGGER = logging.getLogger(__name__)


def add_post_dominators(dot, key, cdg):
    """
    Draw post dominator edges.
    """
    for dst, src, data in cdg.get_post_dominators().edges(data=True):
        tn = angr.utils.graph.TemporaryNode
        if not isinstance(src, tn) and not isinstance(dst, tn):
            LOGGER.info(f"{src} {dst} {data}")
            dot.edge(key(src), key(dst), color="green")


def add_control_deps(dot, key, cdg):
    """
    Draw control dependencies edges.
    """
    for src, dst, data in cdg.graph.edges(data=True):
        LOGGER.debug(f"control dep: {data}")
        dot.edge(key(src), key(dst), color="purple")


def _key(node):
    return str(node.addr)


def _node(dot, node, data, key):
    LOGGER.debug(f"node: {data}")
    dot.node(key(node), node_to_str(node))


def _edge(dot, src, dst, data, key):
    style = "solid"
    if data["jumpkind"] == "Ijk_FakeRet":
        style = "dashed"
    dot.edge(key(src), key(dst), style=style)


def dot_cdg(cdg, cfg, filename="cdg.gv", view=True):
    """
    Draw CDG.

    Edges:

    - Dashed: next instruction after call
    - Green: post-dominator
    - Purple: control dependencies
    """
    dot = draw_graph(cfg.graph, _node, _edge, _key)
    add_post_dominators(dot, _key, cdg)
    add_control_deps(dot, _key, cdg)
    dot.render(view=view, filename=filename)

"""
Module used to handle DDG data_graph.
Contains utilities to simplify it too.
"""

import logging

from angrlab.ddg_simplifier import KILLER, MEM_AFFECT, SOURCE

from .graph import default_node, digraph

LOGGER = logging.getLogger(__name__)


def data_ddg_node(dot, node, data, key=str):
    """
    Displays a simplified ddg node
    """

    dot.node(key(node), data["pp_str"])


def data_ddg_edge(dot, src, dst, data, key):
    """
    Displays a simplified ddg edge
    """

    for edge_type in data["type"]:
        color = (
            "red"
            if edge_type == KILLER
            else "green"
            if edge_type == SOURCE
            else "blue"
            if edge_type == MEM_AFFECT
            else "black"
        )

        dot.edge(key(src), key(dst), color=color)


def dot_data_ddg(graph, filename="data_ddg.gv", view=True):
    """
    Displays a simplified ddg graph
    """

    key = str

    dot = digraph()
    nodes_done = set()
    edges_done = set()

    if not graph.edges():
        LOGGER.warning("No edges in graph")
        for node, data in graph.nodes(data=True):
            default_node(dot, node, data, key)
    for src, dst, data in graph.edges(data=True):
        LOGGER.info(f"{src} {dst} {data}")
        src_data = graph.nodes(data=True)[src]
        data_ddg_node(dot, src, src_data, key)
        nodes_done.add(src)

        dst_data = graph.nodes(data=True)[dst]
        data_ddg_node(dot, dst, dst_data, key)
        nodes_done.add(dst)

        if (src, dst) not in edges_done:
            data_ddg_edge(dot, src, dst, data, key)
            edges_done.add((src, dst))

    for node in filter(lambda node: node not in nodes_done, graph):
        node_data = graph.nodes(data=True)[node]
        data_ddg_node(dot, node, node_data, key)

    dot.render(filename, view=view)

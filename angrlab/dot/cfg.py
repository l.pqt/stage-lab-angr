"""
Module to draw Control Dependency Graph.
"""

import logging

from angrlab.utils import next_addr, node_to_str

from .graph import draw_graph

LOGGER = logging.getLogger(__name__)


def _key(node):
    return str(node.addr)


def _node(dot, node, data, key, **kwargs):
    use_asm = kwargs.get("use_asm", False)
    LOGGER.debug(f"node: {data}")
    dot.node(key(node), node_to_str(node, asm=use_asm))


def _edge(dot, src, dst, data, key, **kwargs):
    """
    Print CFG edges.

    default       : black, solid
    function call : blue, dashed
    function return : blue, solid
    next instruction after call : black, dashed
    conditionnal exec : green/red, solid
    """
    LOGGER.info(f"{src} -> {dst} : {data}")
    LOGGER.debug(kwargs)
    color = "black"
    style = "solid"
    if data["jumpkind"] == "Ijk_Call":
        color = "blue"
        style = "dashed"
    elif data["jumpkind"] == "Ijk_Ret":
        color = "blue"
    elif data["jumpkind"] == "Ijk_FakeRet":
        style = "dashed"
    elif data["jumpkind"] == "Ijk_Boring" and len(src.successors) == 2:
        j_dest = next_addr(src.block)
        if j_dest == dst.addr:
            color = "red"
        else:
            color = "green"
    dot.edge(key(src), key(dst), color=color, style=style)


def dot_cfg(cfg, use_asm=True, filename="cfg.gv", view=True):
    """
    Render CFG.

    - Black, solid: default
    - Blue, dashed: function call
    - Blue, solid: function return
    - Black, dashed: next instruction after return (FakeRet)
    - Green/Red: conditionnal jump (green if jump)
    """
    dot = draw_graph(
        cfg.graph, draw_node=_node, draw_edge=_edge, key=_key, use_asm=use_asm
    )
    dot.render(filename, view=view)

"""
Generic functions to draw graphs in DOT language.
"""

import logging

from graphviz import Digraph

LOGGER = logging.getLogger(__name__)


def digraph():
    """
    Retourne un digraph dot initialisé avec les bons paramètres.
    """
    fontname = "monospace"
    fontsize = "10"
    return Digraph(
        graph_attr={"fontname": fontname, "fontsize": fontsize},
        node_attr={
            "fontname": fontname,
            "fontsize": fontsize,
            "shape": "box",
            "style": "rounded",
        },
        edge_attr={"fontname": fontname, "fontsize": fontsize},
    )


def default_key(node):
    """
    Key for a node.
    """
    return str(hash(node))


def default_node(dot, node, data, key, **kwargs):
    """
    Draw a node in a dot digraph.
    """
    LOGGER.debug(data)
    LOGGER.debug(kwargs)
    dot.node(key(node), str(node))


def default_edge(dot, src, dst, data, key, **kwargs):
    """
    Draw an edge between two nodes in a dot digraph.
    """
    LOGGER.debug(kwargs)
    dot.edge(key(src), key(dst), label=str(data))


def draw_graph(
    graph, draw_node=default_node, draw_edge=default_edge, key=default_key, **kwargs
):
    """
    Draw a networkx digraph as dot digraph.
    """
    dot = digraph()
    if not graph.edges():
        LOGGER.warning("No edges in graph")
        for node, data in graph.nodes(data=True):
            draw_node(dot, node, data, key, **kwargs)
    for src, dst, data in graph.edges(data=True):

        src_data = graph.nodes(data=True)[src]
        draw_node(dot, src, src_data, key, **kwargs)

        dst_data = graph.nodes(data=True)[dst]
        draw_node(dot, dst, dst_data, key, **kwargs)

        draw_edge(dot, src, dst, data, key, **kwargs)
    return dot

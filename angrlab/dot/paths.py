"""
Module to draw paths from arguments to return in a function.
"""

import logging
from random import randint

from angrlab.utils import pairwise

from .graph import digraph

LOGGER = logging.getLogger(__name__)


def _key(node):
    return str(hash(node))


def _node(dot, node):
    dot.node(_key(node), str(node))


def _nodes(dot, paths):
    done = set()
    for path in paths:
        for node in path:
            if node not in done:
                _node(dot, node)


def _path(dot, path):
    r, g, b = [randint(0, 255) for _ in range(3)]  # pylint: disable=invalid-name
    color = f"#{r:x}{g:x}{b:x}"

    for src, dst in pairwise(path):
        LOGGER.info(f"{src} {dst}")
        dot.edge(_key(src), _key(dst), color=color)


def draw_paths(paths, filename="paths.gv", view=True):
    """
    Draw all path between function args and return.
    """
    dot = digraph()
    _nodes(dot, paths)
    for path in paths:
        _path(dot, path)
    dot.render(filename, view=view)

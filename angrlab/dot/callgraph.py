"""
Module to draw (functions) callgraph.
"""

import logging

from .graph import draw_graph

LOGGER = logging.getLogger(__name__)


def _node(dot, node, data, key, **kwargs):
    proj = kwargs.get("proj", None)
    sym = proj.loader.find_symbol(node)
    LOGGER.info(data)
    dot.node(key(node), str(sym))


def dot_callgraph(cfg, filename="callgraph.gv", view=True):
    """
    Draw a callgraph between functions.
    """
    dot = draw_graph(cfg.functions.callgraph, _node, proj=cfg.project)
    dot.render(filename, view=view)

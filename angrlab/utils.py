"""
Functions that are used at many places in angrlab
"""
import logging
from itertools import tee
from typing import Any, Iterable, Iterator, List, Tuple

import angr
from angr.sim_variable import SimRegisterVariable
from networkx.algorithms.simple_paths import all_simple_paths
from networkx.classes.digraph import DiGraph

LOGGER = logging.getLogger(__name__)


def is_allocated(addr: int, state: angr.SimState) -> List[int]:
    """
    Returns the sizes allocated at addr at state state, in Bytes.
    """
    mem_writes = state.history.filter_actions(write_to="mem")

    return [action.size.ast // 8 for action in mem_writes if action.addr.ast == addr]


def get_inst(proj: angr.project.Project, addr: int) -> angr.block.CapstoneInsn:
    """
    Gets the instruction at address addr.

    :param proj: the project
    :param addr: the address
    :return: the instruction @ addr
    """
    block = proj.factory.block(addr)
    inst = block.capstone.insns[0]  # bloc tronqué à l'adresse donnée
    return inst


def next_addr(block: angr.block.Block) -> int:
    """
    Return the address of the block directly after the given block in asm.
    """
    return block.addr + block.size


def asm_block_to_str(block: angr.block.Block) -> str:
    """
    Produces a pretty printable string of asm in the given block.

    :param block: the block
    :return: the pretty printable string
    """
    # \l is for left alignment in DOT
    # pylint: disable=anomalous-backslash-in-string
    return "\l".join([str(inst) for inst in block.capstone.insns]) + "\l"


def vex_block_to_str(block: angr.block.Block) -> str:
    """
    Produces a pretty printable string of the vex in the given block.

    :param block: the block
    :return: the pretty printable string
    """
    # pylint: disable=anomalous-backslash-in-string
    return "\l".join([str(stmt) for stmt in block.vex.statements]) + "\l"


def node_to_str(node: angr.analyses.cfg.CFGNode, asm: bool = True) -> str:
    """
    Wrapper arround `vex_block_to_str` and `asm_block_to_str`.

    :param node: the node you want to have the pretty printable string version of
    :param asm: whether you want asm (default) or vex
    :return: the pretty printable string
    """
    insts = "%#x : %s\n\n" % (node.addr, node.name)
    if not node.block:
        return insts
    if asm:
        insts += asm_block_to_str(node.block)
    else:
        insts += vex_block_to_str(node.block)
    return insts


def get_datapaths(data_graph: DiGraph, var1, var2) -> Iterator[List[Any]]:
    """
    Returns all paths beteween var1 and var2, in data_graph.
    """
    yield from all_simple_paths(data_graph, var1, var2)


def is_real_and_useful(
    node: angr.analyses.code_location.CodeLocation,
    ddg: angr.analyses.ddg.DDG,
    to_exclude: tuple = (),
) -> bool:
    """
    Returns true wether node is an ProgramVariable that you wan't
    to look at (e.g. a valid node in simplified ddg).
    """

    def is_useful(node, ddg, to_exclude):
        """
        A register is useful if it is note excluded.
        Here we can also us angr feature to detect general purpose registers.
        """

        if isinstance(node.variable, SimRegisterVariable):
            return (
                ddg.project.arch.translate_register_name(node.variable.reg)
                not in to_exclude
            )
        return True

    def is_real(node):
        """
        Filters ProgramVariables that are not real (e.g. not VEX temporary).
        """

        return node in list(
            filter(
                lambda node: not isinstance(
                    node.variable, angr.sim_variable.SimTemporaryVariable
                ),
                ddg.data_graph,
            )
        )

    return is_real(node) and is_useful(node, ddg, to_exclude)


def pairwise(iterable: Iterable[Any]) -> Iterator[Tuple[Any, Any]]:
    """
    s -> (s0, s1), (s1, s2), ...

    :param iterable: any iterable
    :return: an iterator on pair from the iterable
    """
    iter_a, iter_b = tee(iterable)
    next(iter_b, None)
    return zip(iter_a, iter_b)


def func_to_addr(proj: angr.Project, name: str) -> int:
    """
    Return the 1st found address of a function.

    proj.kb need to be filled by CFG before.
    Taken from : https://github.com/angr/angr-doc/blob/master/examples/strcpy_find/solve.py.
    """
    found = [addr for addr, func in proj.kb.functions.items() if func.name == name]
    if found != []:
        LOGGER.info(f"Found {len(found)} references to {name}")
        return found[0]
    raise Exception(f"No addr found for function {name}")

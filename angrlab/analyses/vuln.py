"""
Module containing the VulnAnalysis

VulnAnalysis is used to register implemented Vulnerabilities
"""
import logging

import angr

from angrlab.plugins.vuln import VulnCategory, VulnReport, VulnSeverity
from angrlab.stubs import arm_restub

LOGGER = logging.getLogger(__name__)

# Petite note pour faire gaffe au memory read, on peut faire de break points qui vont
# regarder ou on lit/ecrit mais on doit lui donner un event_type, et voila les trucs qu'on doit lui filer
# faut faire state.inspect.add_breakpoint(event_type, ton_bp)
# il faut etendre la classe angr.state_plugins.inspect.BP et rajouter la methode action
# event_types = {
#     'mem_read',
#     'mem_write',
#     'address_concretization',
#     'reg_read',
#     'reg_write',
#     'tmp_read',
#     'tmp_write',
#     'expr',
#     'statement',
#     'instruction',
#     'irsb',
#     'constraints',
#     'exit',
#     'fork',
#     'symbolic_variable'
#     'call',
#     'return',
#     'simprocedure',
#     'syscall',
#     'cfg_handle_job',
#     'vfg_handle_successor',
#     'vfg_widen_state',
#     'engine_process',
#     }


class VulnAnalysis(angr.Analysis):
    """
    A simple analysis that only restubs functions to use "secured" ones
    and then runs SE on it

    :param start_addr: address where the analysis starts
    :param *args: will be passed to the starting state, if *start_addr* is not None
    """

    def __init__(self, start_addr=None, *args, **kwargs):
        arm_restub(self.project)
        self._cfg = self.project.analyses.CFG()
        self._vuln_plugin = self.project.kb.vuln

        if start_addr:
            state = self.project.factory.call_state(
                start_addr,
                add_options={
                    angr.options.SYMBOL_FILL_UNCONSTRAINED_MEMORY,
                    angr.options.SYMBOLIC_WRITE_ADDRESSES,
                    angr.options.SYMBOL_FILL_UNCONSTRAINED_REGISTERS,
                },
                *args,
            )

            # Add extra onstraints on input in initial state
            extra_constraints = kwargs.get("extra_constraints", [])
            for constraint in extra_constraints:
                state.solver.add(constraint)
        else:
            state = self.project.factory.full_init_state()

        try:
            self.check_memory(state)
        except AssertionError:
            LOGGER.warning("Architecture not supported, no memory checks")

        # Veritesting is always enabled, because it only saves us time
        self.simgr = self.project.factory.simgr(state, veritesting=True)

        while self.simgr.active:
            self.simgr.step()
            # self.check_unconstrained()

    def check_unconstrained(self):
        """
        Checks if the current state is unconstrained
        """
        for state in self.simgr.unconstrained:
            LOGGER.warning(f"Found unconstrained state : {state}")
            self._vuln_plugin.register(
                VulnReport(
                    VulnCategory.SYMBOLIC_JUMP, VulnSeverity.CONTROLED, state.copy()
                )
            )

    def check_memory(self, state):
        """Creates breakpoints that check memory at each read (if it is not in the stack)"""

        def mem_read_in_stack(state: angr.SimState):
            """Checks if current memory read is in stack"""

            fp_value = state.solver.eval(state.regs.fp)
            sp_value = state.solver.eval(state.regs.sp)

            if state.inspect.mem_read_address.concrete:
                mem_address_value = state.solver.eval(state.inspect.mem_read_address)

                return fp_value >= mem_address_value >= sp_value
            return True

        def check_valid_read(state: angr.SimState):
            """Checks if the memory read is valid"""
            LOGGER.info(
                f"Memory read {state.inspect.mem_read_length} @ {state.inspect.mem_read_address}"
            )
            self._vuln_plugin.register(
                VulnReport(
                    VulnCategory.BUFFER_OVERFLOW, VulnSeverity.CONTROLED, state.copy()
                )
            )

        assert isinstance(
            state.arch, angr.archinfo.ArchARM
        ), "Architecture not supported"

        state.inspect.b(
            "mem_read",
            when=angr.BP_BEFORE,
            condition=lambda state: not mem_read_in_stack(state),
            action=check_valid_read,
        )

__version__ = "0.1.0"

import angr

from .analyses.vuln import VulnAnalysis

# Register analyses
angr.AnalysesHub.register_default("Vuln", VulnAnalysis)

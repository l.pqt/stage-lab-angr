"""
Module containing the ddg_simplifier

the ddg_simplifier is a function that pull data dependencies between *real* variables
in the default DDG made by angr
"""
import logging

import angr
from angr.sim_variable import (
    SimConstantVariable,
    SimMemoryVariable,
    SimRegisterVariable,
    SimStackVariable,
)
from networkx.classes.digraph import DiGraph

from .utils import is_real_and_useful

LOGGER = logging.getLogger(__name__)


def simvar_pp_str(
    node: angr.analyses.code_location.CodeLocation,
    ddg: angr.analyses.ddg.DDG,
    cfg: angr.analyses.cfg.CFG,
) -> str:
    """
    Returns a pretty-printed version of a ProgramVariable.

    - For registers, retrieves the real name of the register (not VEX name)

    - For Memory and Stack, differentiates both, and prints it the appropriate way

    - For constants, tries to retrieve the value of the constant. It seems to fail if
    the constant is used in a different function than the first block one, but this
    can be caused by bad options during cfg construction

    :param node: The node to have the description of
    :param ddg: A ddg
    :param cfg: A cfg
    :return: A pretty printable string of the code location
    """

    def _node_pp_str(name_part, location_part):
        """
        Little function to fuse name and location strings
        """
        return f"{name_part} : {location_part}"

    # We precompute the location as it is common to every ProgramVariable

    try:
        block = cfg.get_any_node(node.location.block_addr)

        if block is None:
            name = str(block)
        else:
            name = block.name

        location = f"{name} @ {node.location.ins_addr:#x}"
    except Exception:  # We catch all exceptions for the moment
        location = "Not found"

    if isinstance(node.variable, SimRegisterVariable):  # Registers

        # Get the register name
        real_reg_name = ddg.project.arch.translate_register_name(node.variable.reg)
        return _node_pp_str(real_reg_name, location)

    if isinstance(node.variable, SimMemoryVariable):  # Memory and Stack

        # We just compute different names for each data type
        if isinstance(node.variable, SimStackVariable):
            addr_location = f"{node.variable.base}{node.variable.offset:+#x}"
        else:
            addr_location = f"mem:{node.variable.addr:#x}"

        # We use variable size because it was use before
        name = f"{addr_location} ({node.variable.size}B)"

        return _node_pp_str(name, location)

    if isinstance(node.variable, SimConstantVariable):  # Constants

        try:
            # Get the block
            block = cfg.get_any_node(node.location.block_addr).block

            # Get the VEX statement corresponding to current location
            ir_stmt = block.vex.statements[node.location.stmt_idx]

            # Try to get the constant
            constants = ir_stmt.constants

            assert constants != []

            # Not very accurate, but we assume that there is at most one
            # constant per VEX statement
            val = constants[0].value
            return _node_pp_str(f"0x{val:x}", location)

        except AttributeError:
            LOGGER.info(f"Impossible to match block for {node}")
        except AssertionError:
            LOGGER.info(f"Impossible to retrieve constant {node}.")

    return _node_pp_str(str(node.variable), location)


CONSUMER = "consumer"
SOURCE = "source"
KILLER = "kill"
MEM_AFFECT = "mem_data"


def ddg_simplifier(
    ddg: angr.analyses.ddg.DDG,
    cfg: angr.analyses.cfg.CFGEmulated,
    exclude_registers=(),
    exclude_isolated: bool = True,
    exclude_killers: bool = True,
):
    """
    Simplifies a ddg, so that :

    - nodes are just "real" variables
    (e.g. not temporary VEX ones) that you want (not in excluded registers),
    and by default exclude isolated nodes (out_degree == in_degree == 0).

    - edges are just consumer and killer relations between selected nodes
    """

    def pull_useful_nodes(  # pylint: disable=too-many-arguments
        origin,
        node,
        ddg,
        cfg,
        new_graph,
        exclude_registers=exclude_registers,
        exclude_killers=True,
    ):

        #  Lists the edges that we want to follow
        to_follow = ["", "mem_data"] if exclude_killers else ["", "kill", "mem_data"]

        for _, succ, data in ddg.data_graph.out_edges(node, data="type", default=""):

            # data is the type of the edge
            if data in to_follow:
                if is_real_and_useful(succ, ddg, to_exclude=exclude_registers):

                    if data == "":
                        data = CONSUMER

                    LOGGER.debug(f"Adding {origin} -> {succ} : {data}")

                    if not new_graph.has_edge(origin, succ):
                        new_graph.add_edge(origin, succ, type=[data])
                    else:
                        attr_list = new_graph.get_edge_data(origin, succ)["type"]
                        if data not in attr_list:
                            attr_list.append(data)

                    return
                pull_useful_nodes(
                    origin,
                    succ,
                    ddg,
                    cfg,
                    new_graph,
                    exclude_registers=exclude_registers,
                )

    LOGGER.info(f"Simplifying {ddg}")

    new_graph = DiGraph()

    original_graph = ddg.data_graph

    for node in filter(
        lambda node: is_real_and_useful(node, ddg, to_exclude=exclude_registers),
        original_graph,
    ):

        pretty_string = simvar_pp_str(node, ddg, cfg)
        LOGGER.debug(f"Node : {node} ==> {pretty_string}")

        new_graph.add_node(node, pp_str=pretty_string)
        pull_useful_nodes(
            node,
            node,
            ddg,
            cfg,
            new_graph,
            exclude_registers=exclude_registers,
            exclude_killers=exclude_killers,
        )

    for node, data in new_graph.nodes(data=True):
        LOGGER.debug(f"Node : {node} ==> {data}")

    if exclude_isolated:
        nodes_to_exclude = [
            node
            for node in new_graph
            if new_graph.out_degree(node) == 0 and new_graph.in_degree(node) == 0
        ]

        for node in nodes_to_exclude:
            LOGGER.debug(
                f"Excluding isolated node : <{new_graph.nodes(data='pp_str')[node]}>"
            )

        new_graph.remove_nodes_from(nodes_to_exclude)

    return new_graph

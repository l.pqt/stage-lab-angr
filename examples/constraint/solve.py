"""
Constraints

Show how to constraint values before searching vulnerabilities in function.
"""

import angr
import angrlab
import claripy


def main():
    proj = angr.Project("constraint", auto_load_libs=False)

    # get address of function 'f' (need a CFG before)
    proj.analyses.CFGFast()
    f = angrlab.utils.func_to_addr(proj, "f")

    # vulnerability if y == 0
    # call Vuln analyses without parameters on entry point (main)
    proj.analyses.Vuln()

    # pretty-print the potential vulnerability
    vulns = list(proj.kb.vuln[f])
    vulns[0].pprint()

    # analyse f with x unconstrained, y = 42 -> no error
    x = claripy.BVS("x", 32)
    y = claripy.BVV(42, 32)
    args = (x, y)

    # call Vuln analyses with a function address and arguments
    proj.analyses.Vuln(f, *args)

    # same with x unconstrained, y = 0 -> error
    x = claripy.BVS("x", 32)
    y = claripy.BVV(0, 32)
    proj.analyses.Vuln(f, x, y)

    # pretty-print the div by 0
    vulns = list(proj.kb.vuln[f])
    vulns[0].pprint()

    # you can also add constraint before starting the analyse
    x = claripy.BVS("x", 32)
    y = claripy.BVS("y", 32)
    proj.analyses.Vuln(f, x, y, extra_constraints=[x > 3, y != 0])


if __name__ == "__main__":
    main()

	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	arm7tdmi
	.eabi_attribute	6, 2	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"constraint.c"
	.globl	f                       @ -- Begin function f
	.p2align	2
	.type	f,%function
	.code	32                      @ @f
f:
	.fnstart
@ %bb.0:
	.pad	#16
	sub	sp, sp, #16
	mov	r2, r1
	mov	r3, r0
	str	r0, [sp, #12]
	str	r1, [sp, #8]
	ldr	r0, [sp, #12]
	ldr	r1, [sp, #8]
	str	r2, [sp, #4]            @ 4-byte Spill
	str	r3, [sp]                @ 4-byte Spill
	add	sp, sp, #16
	b	__aeabi_idiv
.Lfunc_end0:
	.size	f, .Lfunc_end0-f
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#48
	sub	sp, sp, #48
	mov	r0, #0
	str	r0, [r11, #-4]
	ldr	r1, .LCPI1_0
	str	r0, [r11, #-20]         @ 4-byte Spill
	mov	r0, r1
	bl	printf
	ldr	r1, .LCPI1_1
	sub	lr, r11, #8
	str	r0, [sp, #24]           @ 4-byte Spill
	mov	r0, r1
	str	r1, [sp, #20]           @ 4-byte Spill
	mov	r1, lr
	bl	__isoc99_scanf
	ldr	r1, .LCPI1_2
	str	r0, [sp, #16]           @ 4-byte Spill
	mov	r0, r1
	bl	printf
	sub	r1, r11, #12
	ldr	lr, [sp, #20]           @ 4-byte Reload
	str	r0, [sp, #12]           @ 4-byte Spill
	mov	r0, lr
	bl	__isoc99_scanf
	ldr	r1, [r11, #-8]
	ldr	lr, [r11, #-12]
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r1
	mov	r1, lr
	bl	f
	str	r0, [r11, #-16]
	ldr	r1, [r11, #-16]
	ldr	r0, .LCPI1_3
	bl	printf
	ldr	r1, [r11, #-20]         @ 4-byte Reload
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
	.p2align	2
@ %bb.1:
.LCPI1_0:
	.long	.L.str
.LCPI1_1:
	.long	.L.str.1
.LCPI1_2:
	.long	.L.str.2
.LCPI1_3:
	.long	.L.str.3
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend
                                        @ -- End function
	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"Enter x: "
	.size	.L.str, 10

	.type	.L.str.1,%object        @ @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	.L.str.2,%object        @ @.str.2
.L.str.2:
	.asciz	"Enter y: "
	.size	.L.str.2, 10

	.type	.L.str.3,%object        @ @.str.3
.L.str.3:
	.asciz	"%d\n"
	.size	.L.str.3, 4


	.ident	"clang version 8.0.0 (tags/RELEASE_800/final)"
	.section	".note.GNU-stack","",%progbits
	.addrsig
	.addrsig_sym f
	.addrsig_sym printf
	.addrsig_sym __isoc99_scanf
	.eabi_attribute	30, 6	@ Tag_ABI_optimization_goals

#include <stdlib.h>
#include <stdio.h>

int f(int x, int y) {
	return x / y;
}

int main() {
	int x;
	int y;
	printf("Enter x: ");
	scanf("%d", &x);
	printf("Enter y: ");
	scanf("%d", &y);

	int res = f(x, y);
	printf("%d\n", res);

	return 0;
}

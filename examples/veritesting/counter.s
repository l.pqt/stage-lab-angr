	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	arm7tdmi
	.eabi_attribute	6, 2	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"counter.c"
	.globl	bug                     @ -- Begin function bug
	.p2align	2
	.type	bug,%function
	.code	32                      @ @bug
bug:
	.fnstart
@ %bb.0:
	.pad	#4
	sub	sp, sp, #4
	ldr	r1, [sp]
	mov	r0, #1
	add	sp, sp, #4
	b	__aeabi_idiv
.Lfunc_end0:
	.size	bug, .Lfunc_end0-bug
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	f                       @ -- Begin function f
	.p2align	2
	.type	f,%function
	.code	32                      @ @f
f:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	mov	r1, r0
	str	r0, [r11, #-4]
	mov	r0, #0
	str	r0, [r11, #-8]
	str	r0, [sp, #12]
	str	r0, [sp, #8]
	str	r1, [sp, #4]            @ 4-byte Spill
	b	.LBB1_1
.LBB1_1:                                @ =>This Inner Loop Header: Depth=1
	ldr	r0, [sp, #8]
	cmp	r0, #99
	bgt	.LBB1_6
	b	.LBB1_2
.LBB1_2:                                @   in Loop: Header=BB1_1 Depth=1
	ldr	r0, [r11, #-4]
	ldr	r1, [sp, #8]
	ldrb	r0, [r0, r1]
	cmp	r0, #66
	bne	.LBB1_4
	b	.LBB1_3
.LBB1_3:                                @   in Loop: Header=BB1_1 Depth=1
	ldr	r0, [r11, #-8]
	add	r0, r0, #1
	str	r0, [r11, #-8]
	ldr	r0, [sp, #12]
	add	r0, r0, #2
	str	r0, [sp, #12]
	b	.LBB1_4
.LBB1_4:                                @   in Loop: Header=BB1_1 Depth=1
	b	.LBB1_5
.LBB1_5:                                @   in Loop: Header=BB1_1 Depth=1
	ldr	r0, [sp, #8]
	add	r0, r0, #1
	str	r0, [sp, #8]
	b	.LBB1_1
.LBB1_6:
	ldr	r0, [r11, #-8]
	cmp	r0, #75
	bne	.LBB1_8
	b	.LBB1_7
.LBB1_7:
	bl	bug
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB1_8
.LBB1_8:
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end1:
	.size	f, .Lfunc_end1-f
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#144
	sub	sp, sp, #144
	mov	r0, #0
	str	r0, [r11, #-4]
	ldr	r1, .LCPI2_0
	add	r2, sp, #12
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, r1
	mov	r1, r2
	str	r2, [sp, #4]            @ 4-byte Spill
	bl	__isoc99_scanf
	ldr	r1, [sp, #4]            @ 4-byte Reload
	str	r0, [sp]                @ 4-byte Spill
	mov	r0, r1
	bl	f
	ldr	r0, [sp, #8]            @ 4-byte Reload
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
	.p2align	2
@ %bb.1:
.LCPI2_0:
	.long	.L.str
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cantunwind
	.fnend
                                        @ -- End function
	.type	.L.str,%object          @ @.str
	.section	.rodata.str1.1,"aMS",%progbits,1
.L.str:
	.asciz	"%s"
	.size	.L.str, 3


	.ident	"clang version 8.0.0 (tags/RELEASE_800/final)"
	.section	".note.GNU-stack","",%progbits
	.addrsig
	.addrsig_sym bug
	.addrsig_sym f
	.addrsig_sym __isoc99_scanf
	.eabi_attribute	30, 6	@ Tag_ABI_optimization_goals

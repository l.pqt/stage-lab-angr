#include <stdio.h>

int bug() {
	int zero;
	return 1 / zero;
}

void f(char *input) {
	int counter = 0;
	int values = 0;
	for (int i = 0; i< 100; i++) {
		if (input[i] == 'B') {
			counter++;
			values += 2;
		}
	}

	if (counter == 75) {
		bug();
	}
}

int main() {
	char input[128];
	scanf("%s", input);
	f(input);
	return 0;
}

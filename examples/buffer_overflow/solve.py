import angr
import angrlab
import logging

def main():

    # Add more verbosity when running the script
    angrlab_logger = logging.getLogger("angrlab")
    angrlab_logger.setLevel("INFO")

    # As we load an exectutable, we don't want to load libraries
    proj = angr.Project("./buffer_overflow", auto_load_libs=False)

    # Type <C-C> when you are borred, what you are going to see on the screen is
    # the deteced vulnerabilities

    print("Starting to analyse, press Ctrl-C to stop")
    input("Press enter to start...")
    try:
        proj.analyses.Vuln()
    except KeyboardInterrupt:
        print("Stopping analysis")

    print("We discovered some vulnerabilities :")
    print(proj.kb.vuln)

    print("Here the first one :")
    print(list(proj.kb.vuln)[0])

if __name__ == '__main__':
    main()

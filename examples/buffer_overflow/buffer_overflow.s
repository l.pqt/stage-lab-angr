	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	arm7tdmi
	.eabi_attribute	6, 2	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"buffer_overflow.c"
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#48
	sub	sp, sp, #48
	mov	r2, r1
	mov	r3, r0
	mov	r12, #0
	str	r12, [r11, #-4]
	str	r0, [r11, #-8]
	str	r1, [r11, #-12]
	mov	r0, #32
	mov	r1, #4
	str	r1, [sp, #16]           @ 4-byte Spill
	str	r2, [sp, #12]           @ 4-byte Spill
	str	r3, [sp, #8]            @ 4-byte Spill
	str	r12, [sp, #4]           @ 4-byte Spill
	bl	calloc
	str	r0, [r11, #-16]
	ldr	r0, [r11, #-12]
	ldr	r0, [r0, #4]
	bl	atoi
	str	r0, [r11, #-20]
	ldr	r0, [r11, #-20]
	ldr	r1, [sp, #16]           @ 4-byte Reload
	bl	calloc
	str	r0, [sp, #24]
	ldr	r0, [sp, #4]            @ 4-byte Reload
	str	r0, [sp, #20]
	b	.LBB0_1
.LBB0_1:                                @ =>This Inner Loop Header: Depth=1
	ldr	r0, [sp, #20]
	ldr	r1, [r11, #-20]
	cmp	r0, r1
	bhs	.LBB0_4
	b	.LBB0_2
.LBB0_2:                                @   in Loop: Header=BB0_1 Depth=1
	ldr	r0, [sp, #20]
	ldr	r1, [sp, #24]
	str	r0, [r1, r0, lsl #2]
	b	.LBB0_3
.LBB0_3:                                @   in Loop: Header=BB0_1 Depth=1
	ldr	r0, [sp, #20]
	add	r0, r0, #1
	str	r0, [sp, #20]
	b	.LBB0_1
.LBB0_4:
	ldr	r0, [r11, #-16]
	ldr	r1, [sp, #24]
	ldr	r2, [r11, #-20]
	lsl	r2, r2, #2
	bl	memcpy
	ldr	r1, [r11, #-16]
	str	r0, [sp]                @ 4-byte Spill
	mov	r0, r1
	bl	free
	ldr	r0, [sp, #24]
	bl	free
	mov	r0, #0
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cantunwind
	.fnend
                                        @ -- End function
	.type	BUFFER_SIZE,%object     @ @BUFFER_SIZE
	.section	.rodata,"a",%progbits
	.globl	BUFFER_SIZE
	.p2align	2
BUFFER_SIZE:
	.long	32                      @ 0x20
	.size	BUFFER_SIZE, 4


	.ident	"clang version 8.0.0 (tags/RELEASE_800/final)"
	.section	".note.GNU-stack","",%progbits
	.addrsig
	.addrsig_sym calloc
	.addrsig_sym atoi
	.addrsig_sym free
	.eabi_attribute	30, 6	@ Tag_ABI_optimization_goals

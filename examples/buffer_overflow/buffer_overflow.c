#include <stdlib.h>
#include <string.h>

const size_t BUFFER_SIZE = 32;

int main(int argc, char **argv) {
	int *dst = calloc(BUFFER_SIZE, sizeof(int));

	int size = atoi(argv[1]);
	int *src = calloc(size, sizeof(int));
	for (size_t i = 0; i < size; i++) {
		src[i] = i;
	}

	/* buffer overflow if argv[1] > 32 */
	memcpy(dst, src, size * sizeof(int));

	free(dst);
	free(src);
	return 0;
}

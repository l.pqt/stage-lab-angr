#include <stdlib.h>
#include <string.h>

const size_t BUFFER_SIZE = 32;

void f(int *src, size_t size) {
	int *dst = calloc(BUFFER_SIZE, sizeof(int));
	memcpy(dst, src, size * sizeof(int));
}

void f_secure(int *src, size_t size) {
	int *dst = calloc(BUFFER_SIZE, sizeof(int));
	if (size > BUFFER_SIZE) {
		return;
	}
	else {
		memcpy(dst, src, size * sizeof(int));
	}
}

void f_error() {
	size_t size = BUFFER_SIZE + 1;
	int *dst = calloc(BUFFER_SIZE, sizeof(int));
	int *src = calloc(size, sizeof(int));
	memcpy(dst, src, size * sizeof(int));
}

int main() {
	return 0;
}

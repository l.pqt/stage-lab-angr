	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	arm7tdmi
	.eabi_attribute	6, 2	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"memcpy.c"
	.globl	f                       @ -- Begin function f
	.p2align	2
	.type	f,%function
	.code	32                      @ @f
f:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	mov	r2, r1
	mov	r3, r0
	str	r0, [r11, #-4]
	str	r1, [r11, #-8]
	mov	r0, #32
	mov	r1, #4
	str	r2, [sp, #8]            @ 4-byte Spill
	str	r3, [sp, #4]            @ 4-byte Spill
	bl	calloc
	str	r0, [sp, #12]
	ldr	r0, [sp, #12]
	ldr	r1, [r11, #-4]
	ldr	r2, [r11, #-8]
	lsl	r2, r2, #2
	bl	memcpy
	str	r0, [sp]                @ 4-byte Spill
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end0:
	.size	f, .Lfunc_end0-f
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	f_secure                @ -- Begin function f_secure
	.p2align	2
	.type	f_secure,%function
	.code	32                      @ @f_secure
f_secure:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	mov	r2, r1
	mov	r3, r0
	str	r0, [r11, #-4]
	str	r1, [r11, #-8]
	mov	r0, #32
	mov	r1, #4
	str	r2, [sp, #8]            @ 4-byte Spill
	str	r3, [sp, #4]            @ 4-byte Spill
	bl	calloc
	str	r0, [sp, #12]
	ldr	r0, [r11, #-8]
	cmp	r0, #33
	blo	.LBB1_2
	b	.LBB1_1
.LBB1_1:
	b	.LBB1_3
.LBB1_2:
	ldr	r0, [sp, #12]
	ldr	r1, [r11, #-4]
	ldr	r2, [r11, #-8]
	lsl	r2, r2, #2
	bl	memcpy
	str	r0, [sp]                @ 4-byte Spill
	b	.LBB1_3
.LBB1_3:
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end1:
	.size	f_secure, .Lfunc_end1-f_secure
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	f_error                 @ -- Begin function f_error
	.p2align	2
	.type	f_error,%function
	.code	32                      @ @f_error
f_error:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	mov	r0, #33
	str	r0, [r11, #-4]
	mov	r0, #32
	mov	r1, #4
	str	r1, [sp, #8]            @ 4-byte Spill
	bl	calloc
	str	r0, [r11, #-8]
	ldr	r0, [r11, #-4]
	ldr	r1, [sp, #8]            @ 4-byte Reload
	bl	calloc
	str	r0, [sp, #12]
	ldr	r0, [r11, #-8]
	ldr	r1, [sp, #12]
	ldr	lr, [r11, #-4]
	lsl	r2, lr, #2
	bl	memcpy
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end2:
	.size	f_error, .Lfunc_end2-f_error
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	.pad	#4
	sub	sp, sp, #4
	mov	r0, #0
	str	r0, [sp]
	add	sp, sp, #4
	bx	lr
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cantunwind
	.fnend
                                        @ -- End function
	.type	BUFFER_SIZE,%object     @ @BUFFER_SIZE
	.section	.rodata,"a",%progbits
	.globl	BUFFER_SIZE
	.p2align	2
BUFFER_SIZE:
	.long	32                      @ 0x20
	.size	BUFFER_SIZE, 4


	.ident	"clang version 8.0.0 (tags/RELEASE_800/final)"
	.section	".note.GNU-stack","",%progbits
	.addrsig
	.addrsig_sym calloc
	.eabi_attribute	30, 6	@ Tag_ABI_optimization_goals

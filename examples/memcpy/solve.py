import logging

import angr
import angrlab
import claripy

"""
In memcpy, we have 3 functions :
    - one that may cause buffer overflows
    - one that will always cause a buffer overflow
    - the last one that has been secured

Running the following script will perform the analysis
on the first function of the binary (f the first in the above list)
"""

def main():
    proj = angr.Project("./memcpy.o")

    # Perform the analysis
    proj.analyses.CFGFast()
    print(list(proj.kb.functions.values()))
    f = angrlab.utils.func_to_addr(proj, "f")


    input("\n\nPress ENTER to start analysing...")
    src = claripy.BVS("src", 32)
    size = claripy.BVS("size", 32)
    proj.analyses.Vuln(f, src, size)

    # If we've found some vulnerabilities
    if len(list(proj.kb.vuln)) > 0:
        print("Found vulns !!")
        print(proj.kb.vuln)

        # Display informations about a chosen one
        real_vuln = list(proj.kb.vuln)[0]
        print(f"The first one is :")
        real_vuln.pprint()

if __name__ == '__main__':
    main()

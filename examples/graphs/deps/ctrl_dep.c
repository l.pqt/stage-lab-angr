/**
 * Résultat dépend directement de y et indirectement de x, pas n.
 */
int f(int x, int y, int n) {
	int z;
	/* dépendance de contrôle sur x */
	if (x > 5) {
		z = 1;
	}
	else {
		z = 2;
	}

	/* n'affecte rien */
	int a = n + 2;

	return z + y;
}

int main() {
	return 0;
}

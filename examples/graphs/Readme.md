# `./graphs.y` examples

All the examples in this directory are meant to be used
with `graph.py`, they each represent a certain object or dependency we
wanted to visualize

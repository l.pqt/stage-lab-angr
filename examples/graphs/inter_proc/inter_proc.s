	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	arm7tdmi
	.eabi_attribute	6, 2	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"inter_proc.c"
	.globl	add                     @ -- Begin function add
	.p2align	2
	.type	add,%function
	.code	32                      @ @add
add:
	.fnstart
@ %bb.0:
	.pad	#8
	sub	sp, sp, #8
	mov	r1, r0
	str	r0, [sp, #4]
	ldr	r0, [sp, #4]
	add	r0, r0, #1
	str	r1, [sp]                @ 4-byte Spill
	add	sp, sp, #8
	bx	lr
.Lfunc_end0:
	.size	add, .Lfunc_end0-add
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	cst                     @ -- Begin function cst
	.p2align	2
	.type	cst,%function
	.code	32                      @ @cst
cst:
	.fnstart
@ %bb.0:
	.pad	#8
	sub	sp, sp, #8
	mov	r1, r0
	str	r0, [sp, #4]
	mov	r0, #1
	str	r1, [sp]                @ 4-byte Spill
	add	sp, sp, #8
	bx	lr
.Lfunc_end1:
	.size	cst, .Lfunc_end1-cst
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	f                       @ -- Begin function f
	.p2align	2
	.type	f,%function
	.code	32                      @ @f
f:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#24
	sub	sp, sp, #24
	mov	r2, r1
	mov	r3, r0
	str	r0, [r11, #-4]
	str	r1, [r11, #-8]
	ldr	r0, [r11, #-4]
	str	r2, [sp, #4]            @ 4-byte Spill
	str	r3, [sp]                @ 4-byte Spill
	bl	add
	str	r0, [sp, #12]
	ldr	r0, [r11, #-8]
	bl	cst
	str	r0, [sp, #8]
	ldr	r0, [sp, #12]
	ldr	r1, [sp, #8]
	add	r0, r0, r1
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end2:
	.size	f, .Lfunc_end2-f
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	.pad	#4
	sub	sp, sp, #4
	mov	r0, #0
	str	r0, [sp]
	add	sp, sp, #4
	bx	lr
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cantunwind
	.fnend
                                        @ -- End function

	.ident	"clang version 8.0.0 (tags/RELEASE_800/final)"
	.section	".note.GNU-stack","",%progbits
	.addrsig
	.addrsig_sym add
	.addrsig_sym cst
	.eabi_attribute	30, 6	@ Tag_ABI_optimization_goals

int add(int n) {
	return n + 1;
}

int cst(int n) {
	return 1;
}

int f(int a, int b) {
	int x = add(a);
	int y = cst(b);

	return x + y;
}

int main() {
	return 0;
}

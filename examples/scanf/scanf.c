#include <stdlib.h>
#include <stdio.h>


int divi(int x) {
	return 1 / (x - 4);
}


int main() {
	printf("Number: ");
	int n;
	scanf("%d", &n);
	int res = divi(n);
	printf("1 / %d = %d\n", n, res);

	return 0;
}

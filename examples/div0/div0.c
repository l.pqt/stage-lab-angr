int f(int x) {
	return 1 / x;
}

int f_secure(int x) {
	if (x != 0) {
		return 1 / x;
	}
	else {
		return 0;
	}
}


void test(int x) {
	f(x);
	f_secure(x);
}

int main() {
	f(0);
	return 0;
}

#!/usr/bin/env python3

import sys
import angr

import angrlab


def main():
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        filename = "div0"

    # import project
    proj = angr.Project(filename, auto_load_libs=False)

    # analyse for vulnerabilities from main
    # should always show a division by 0 as we call f(0)
    proj.analyses.Vuln()

    # get functions addresses
    funcs = ["f", "f_secure", "test", "main"]
    addrs = {f: angrlab.utils.func_to_addr(proj, f) for f in funcs}
    f = addrs["f"]
    f_sec = addrs["f_secure"]

    # no vuln in f_secure
    assert f_sec not in proj.kb.vuln

    # f has a potential division by 0
    print(proj.kb.vuln)
    print(hex(f))

    # Get a summary for a vulnerability in f
    proj.kb.vuln[f][0].pprint()


if __name__ == "__main__":
    main()

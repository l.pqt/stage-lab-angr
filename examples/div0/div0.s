	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	arm7tdmi
	.eabi_attribute	6, 2	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"div0.c"
	.globl	f                       @ -- Begin function f
	.p2align	2
	.type	f,%function
	.code	32                      @ @f
f:
	.fnstart
@ %bb.0:
	.pad	#12
	sub	sp, sp, #12
	mov	r1, r0
	str	r0, [sp, #8]
	ldr	r0, [sp, #8]
	mov	r2, #1
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r2
	ldr	r2, [sp, #4]            @ 4-byte Reload
	str	r1, [sp]                @ 4-byte Spill
	mov	r1, r2
	add	sp, sp, #12
	b	__aeabi_idiv
.Lfunc_end0:
	.size	f, .Lfunc_end0-f
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	f_secure                @ -- Begin function f_secure
	.p2align	2
	.type	f_secure,%function
	.code	32                      @ @f_secure
f_secure:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	mov	r1, r0
	str	r0, [sp, #8]
	ldr	r0, [sp, #8]
	cmp	r0, #0
	str	r1, [sp, #4]            @ 4-byte Spill
	beq	.LBB1_2
	b	.LBB1_1
.LBB1_1:
	ldr	r1, [sp, #8]
	mov	r0, #1
	bl	__aeabi_idiv
	str	r0, [r11, #-4]
	b	.LBB1_3
.LBB1_2:
	mov	r0, #0
	str	r0, [r11, #-4]
	b	.LBB1_3
.LBB1_3:
	ldr	r0, [r11, #-4]
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end1:
	.size	f_secure, .Lfunc_end1-f_secure
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	test                    @ -- Begin function test
	.p2align	2
	.type	test,%function
	.code	32                      @ @test
test:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	mov	r1, r0
	str	r0, [r11, #-4]
	ldr	r0, [r11, #-4]
	str	r1, [sp, #8]            @ 4-byte Spill
	bl	f
	ldr	r1, [r11, #-4]
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, r1
	bl	f_secure
	str	r0, [sp]                @ 4-byte Spill
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end2:
	.size	test, .Lfunc_end2-test
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#16
	sub	sp, sp, #16
	mov	r0, #0
	str	r0, [r11, #-4]
	str	r0, [sp, #8]            @ 4-byte Spill
	bl	f
	ldr	lr, [sp, #8]            @ 4-byte Reload
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, lr
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cantunwind
	.fnend
                                        @ -- End function

	.ident	"clang version 8.0.0 (tags/RELEASE_800/final)"
	.section	".note.GNU-stack","",%progbits
	.addrsig
	.addrsig_sym f
	.addrsig_sym f_secure
	.eabi_attribute	30, 6	@ Tag_ABI_optimization_goals

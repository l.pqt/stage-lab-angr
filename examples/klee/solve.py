"""
Test on KLEE examples.
"""

import angr
import angrlab
import claripy


def ex2():
    proj = angr.Project("ex2.o")

    # get function f(x, y, z) to analyse
    proj.analyses.CFGFast()
    f = angrlab.utils.func_to_addr(proj, "f")


    x = claripy.BVS("x", 32)
    y = claripy.BVS("y", 32)
    z = claripy.BVS("z", 32)

    proj.analyses.Vuln(f, x, y, z)


if __name__ == '__main__':
    ex2()

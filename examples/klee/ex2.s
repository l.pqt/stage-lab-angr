	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	arm7tdmi
	.eabi_attribute	6, 2	@ Tag_CPU_arch
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 1	@ Tag_THUMB_ISA_use
	.eabi_attribute	34, 0	@ Tag_CPU_unaligned_access
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 1	@ Tag_ABI_FP_denormal
	.eabi_attribute	21, 1	@ Tag_ABI_FP_exceptions
	.eabi_attribute	23, 3	@ Tag_ABI_FP_number_model
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.file	"ex2.c"
	.globl	f                       @ -- Begin function f
	.p2align	2
	.type	f,%function
	.code	32                      @ @f
f:
	.fnstart
@ %bb.0:
	.save	{r11, lr}
	push	{r11, lr}
	.setfp	r11, sp
	mov	r11, sp
	.pad	#32
	sub	sp, sp, #32
	mov	r3, r2
	mov	r12, r1
	mov	lr, r0
	str	r0, [r11, #-4]
	str	r1, [r11, #-8]
	str	r2, [r11, #-12]
	ldr	r0, [r11, #-4]
	ldr	r1, [r11, #-12]
	mul	r2, r1, r1
	cmp	r0, r2
	str	r3, [sp, #8]            @ 4-byte Spill
	str	r12, [sp, #4]           @ 4-byte Spill
	str	lr, [sp]                @ 4-byte Spill
	bne	.LBB0_2
	b	.LBB0_1
.LBB0_1:
	mov	r0, #3
	str	r0, [r11, #-8]
	ldr	r0, [r11, #-8]
	add	r1, sp, #12
	mov	r2, #0
	str	r2, [r1, r0, lsl #2]
	b	.LBB0_3
.LBB0_2:
	ldr	r0, [r11, #-4]
	str	r0, [r11, #-8]
	b	.LBB0_3
.LBB0_3:
	mov	sp, r11
	pop	{r11, lr}
	bx	lr
.Lfunc_end0:
	.size	f, .Lfunc_end0-f
	.cantunwind
	.fnend
                                        @ -- End function
	.globl	main                    @ -- Begin function main
	.p2align	2
	.type	main,%function
	.code	32                      @ @main
main:
	.fnstart
@ %bb.0:
	.pad	#4
	sub	sp, sp, #4
	mov	r0, #0
	str	r0, [sp]
	add	sp, sp, #4
	bx	lr
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cantunwind
	.fnend
                                        @ -- End function

	.ident	"clang version 8.0.0 (tags/RELEASE_800/final)"
	.section	".note.GNU-stack","",%progbits
	.addrsig
	.eabi_attribute	30, 6	@ Tag_ABI_optimization_goals

#include <stdio.h>
#include <stdlib.h>
//#include "/home/klee/klee_src/include/klee/klee.h"

// global variables = test inputs

void f(int x, int y, int z) {
  int T[2];

  // klee_make_symbolic(&x, sizeof(x), "x") ;
  // klee_assume(x>5) ;
  // klee_make_symbolic(&z, sizeof(z), "z") ;
  if (x == z * z) {
    y = 3;
    T[y] = 0;
  } else {
    y = x;
  }
}

int main() {
	return 0;
}

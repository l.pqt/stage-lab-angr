import logging

import angr
import angrlab
import claripy

def main():
    proj = angr.Project("./loop", auto_load_libs=False)


    input("\n\nPress ENTER to start analysing...")
    proj.analyses.Vuln()

    # If we've found some vulnerabilities
    if len(list(proj.kb.vuln)) > 0:
        print("Found vulns !!")
        print(proj.kb.vuln)

        # Display informations about a chosen one
        real_vuln = list(proj.kb.vuln)[0]
        print(f"The first one is :")
        real_vuln.pprint()

if __name__ == '__main__':
    main()

FROM archlinux/base

# Install packages
RUN pacman -Sy --noconfirm --needed base-devel python python-pip gcc libffi man-db
RUN pip3 install --upgrade pip

# Setup angrlab user
RUN useradd angrlab -m
WORKDIR /home/angrlab

# Install angrlab
COPY angrlab angrlab
COPY pyproject.toml poetry.lock ./
RUN pip install .
RUN pip install ipython

# Copy examples & scripts
COPY examples examples
COPY doc doc
COPY graphs.py README.md ./

# Install doc as man pages
RUN cd doc && make man
RUN mkdir -p /usr/local/share/man/man1
RUN cp doc/_build/man/angrlab.1 /usr/local/share/man/man1/
RUN mandb

USER angrlab
CMD bash

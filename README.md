# AngrLab

AngrLab is the result of an internship at [Verimag](https://www-verimag.imag.fr/). It contains tests and
experimentations on what can be achieved with [angr](http://angr.io/), mainly on ARM binaries.

This project is split in 2 parts:

- Static analyses
- Dynamic Symbolic Execution

## Static analyses

`graphs.py` is a script to display visualizations (with *Graphviz*) of some static analyses built-in *angr*.

## Dynamic Symbolic Execution

We tried to implement a new analisis named Vuln.
All the analysis is stored in `analyses` folder, and it relies under the hood
on VulnPlugin in the `plugin` directory, and on the stubs (in the `stubs` directory).

## Quickstart

```sh
# Clone the project
git clone https://gitlab.com/l.pqt/stage-lab-angr angrlab
cd angrlab

# if needed, install poetry
pip install --user poetry

# Install python dependencies in virtualenv
poetry install

# Open a shell in virtualenv
poetry shell

# Open an interactive python shell
ipython
```

In IPython:

```python
import angr
import angrlab

proj = angr.Project("/path/to/bin")
proj.analyses.Vuln()
```

## Documentation

The documentation is built with *sphinx*. To build HTML doc:

```sh
make doc
firefox doc/_build/html/index.html
```

## Docker container

The fastest way to use AngrLab, is to pull the image from Docker Hub.

```sh
docker run -it lpqt/angrlab
```

You can also build the container locally:

```sh
docker build . -t angrlab
docker run -it angrlab
```

To use AngrLab with your own binaries, mount a volume in the container:

```sh
docker run -it -v /path/to/local/dir:/home/angrlab/mnt angrlab
```

In the container, the documentation is available as man pages.

```sh
man angrlab
```

We also provide some example in `examples` with binaries and the corresponding script (`solve.py`).
